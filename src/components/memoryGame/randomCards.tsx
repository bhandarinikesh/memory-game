export const TOTAL_CARDS = 4;
const CARDNUMBERS = [...Array(TOTAL_CARDS / 2).keys()].map((i) => i + 1);

const randomCards = () => {
  const randomcard = CARDNUMBERS.concat(CARDNUMBERS).sort(
    () => 0.5 - Math.random()
  );
  return randomcard.map((value, index) => ({
    id: index,
    value,
    isFlipped: false,
    isMatched: false,
  }));
};
export default randomCards;
