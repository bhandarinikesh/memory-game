import { lazy } from "react";

export const GameLayout = lazy(() => import("../components/layout/GameLayout"));
export const MemoryGame = lazy(() => import("../components/memoryGame/index"));
export const NotFound = lazy(() => import("../components/pageNotfound"));
