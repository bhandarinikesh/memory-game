import React from "react";
import { MemoryGame, GameLayout, NotFound } from "./LazyRoute";

export const AllRoutes = () => [
  {
    path: "/",
    element: <GameLayout />,
    children: [{ path: "/home", element: <MemoryGame /> }],
  },

  { path: "*", element: <NotFound /> },
];
